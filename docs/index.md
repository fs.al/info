---
title: fs.al
hide:
  - navigation
  - toc
---

<base target="_blank">

# fs.al

## Edu

* [x] [https://edu.fs.al](https://edu.fs.al) (moodle)
* [x] [https://ocw.fs.al](https://ocw.fs.al) (moodle)
* [x] [https://vclab.fs.al](https://vclab.fs.al) (guacamole)
* [x] [https://bbb.fs.al](https://bbb.fs.al) (big-blue-button)
* [x] [https://linux-cli.fs.al](https://linux-cli.fs.al) (docusaurus)

## Social

* [x] [https://toot.fs.al](https://toot.fs.al) (mastodon)
* [x] [https://fol.fs.al](https://fol.fs.al) (discourse)
* [x] [https://mm.fs.al](https://mm.fs.al) (mattermost)
* [x] [https://chat.fs.al](https://chat.fs.al) (snikket)
* [x] [https://events.fs.al](https://events.fs.al) (indico)
* [x] [https://galene.fs.al](https://galene.fs.al) (galene)
* [ ] [https://talk.fs.al](https://talk.fs.al) (talkyard)

## Collab

* [x] [https://cloud.fs.al](https://cloud.fs.al) (nextcloud)
* [ ] [https://matrix.fs.al](https://matrix.fs.al) (synapse)
* [ ] [https://feedback.fs.al](https://feedback.fs.al) (liquid-feedback)
* [ ] [https://gitea.fs.al](https://gitea.fs.al) (gitea)

## Translate

* [x] [https://fjalori.fs.al](https://fjalori.fs.al)
* [x] [https://l10n.fs.al](https://l10n.fs.al)
* [x] [https://btranslator.fs.al](https://btranslator.fs.al)
* [x] [https://qtranslate.fs.al](https://qtranslate.fs.al)

## Books

* [x] [http://p101.fs.al](http://p101.fs.al/)
* [x] [https://even-angels-ask.books.fs.al](https://even-angels-ask.books.fs.al)
* [x] [https://letersi.gitlab.io/alice-in-wonderland](https://letersi.gitlab.io/alice-in-wonderland)
* [x] [https://letersi.gitlab.io/naim-frasheri](https://letersi.gitlab.io/naim-frasheri)
* [x] [https://letersi.gitlab.io/bageti-e-bujqesi](https://letersi.gitlab.io/bageti-e-bujqesi)
* [x] [https://letersi.gitlab.io/even-angels-ask](https://letersi.gitlab.io/even-angels-ask)
* [x] [https://letersi.gitlab.io/vjersha](https://letersi.gitlab.io/vjersha)

<script defer data-domain="fs.al" src="https://analytics.fs.al/js/script.js"></script>
